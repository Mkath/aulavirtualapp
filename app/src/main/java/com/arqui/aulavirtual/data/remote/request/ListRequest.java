package com.arqui.aulavirtual.data.remote.request;

import com.arqui.aulavirtual.data.entities.CourseEntity;
import com.arqui.aulavirtual.data.entities.HomeworksEntity;
import com.arqui.aulavirtual.data.entities.UserByJob;
import com.arqui.aulavirtual.data.entities.trackholder.TrackEntityHolder;
import com.arqui.aulavirtual.data.entities.trackholder.TrackEntityHolderUser;


import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Created by katherine on 12/06/17.
 */

public interface ListRequest {
    //cursos
    @GET("webservice/rest/server.php")
    Call<ArrayList<CourseEntity>> getCourses(@Query("wstoken") String token,
                                             @Query("moodlewsrestformat") String format,
                                             @Query("wsfunction") String function,
                                             @Query("userid") int userid);

    //tareas
    @GET("webservice/rest/server.php")
    Call<ArrayList<HomeworksEntity>> getHomeworks(@Query("wstoken") String token,
                                                  @Query("moodlewsrestformat") String format,
                                                  @Query("courseid") String courseid,
                                                  @Query("wsfunction") String function);

    @GET("admin/jobs/{id}/applicationoffer")
    Call<TrackEntityHolderUser<UserByJob>> getUsersByJobs(@Path("id") int idJob);

}
