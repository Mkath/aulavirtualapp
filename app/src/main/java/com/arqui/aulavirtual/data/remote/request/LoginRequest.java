package com.arqui.aulavirtual.data.remote.request;


import com.arqui.aulavirtual.data.entities.AccessTokenEntity;
import com.arqui.aulavirtual.data.entities.UserEntity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by katherine on 10/05/17.
 */

public interface LoginRequest {
    @GET("webservice/rest/server.php")
    Call<ArrayList<UserEntity>> login(@Query("wstoken") String token,
                          @Query("moodlewsrestformat") String format,
                          @Query("wsfunction") String service,
                          @Query("field") String field,
                          @Query("values[0]") String username);



}
