package com.arqui.aulavirtual.data.entities.trackholder;

import java.util.ArrayList;

/**
 * Created by miguel on 7/04/17.
 */

public class TrackEntityHolderUser<T> {
    private ArrayList<T> users;

    public ArrayList<T> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<T> users) {
        this.users = users;
    }
}
