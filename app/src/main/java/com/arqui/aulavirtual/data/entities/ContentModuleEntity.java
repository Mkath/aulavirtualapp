package com.arqui.aulavirtual.data.entities;

import java.io.Serializable;

public class ContentModuleEntity implements Serializable {
    private String filename;
    private String fileurl;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFileurl() {
        return fileurl;
    }

    public void setFileurl(String fileurl) {
        this.fileurl = fileurl;
    }

    /*"id": 1,
                "url": "http://35.239.193.129/moodle/mod/forum/view.php?id=1",
                "name": "Avisos",
                "instance": 1,
                "visible": 1,
                "uservisible": true,
                "visibleoncoursepage": 1,
                "modicon": "http://35.239.193.129/moodle/theme/image.php/moove/forum/1559269261/icon",
                "modname": "forum",
                "modplural": "Foros",
                "availability": null,
                "indent": 0,
                "onclick": "",
                "afterlink": null,
                "customdata": "\"\"",
                "completion": 0*/

    /*/* public String getExpiration(){
        if (getJobOfferExpiration() == null ){
            return "";
        }
        Date tempDate = null;
        SimpleDateFormat parseDateFromServer= new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat parseDateForShowDetail =  new SimpleDateFormat("dd' de 'MMMM' del 'yyyy", new Locale("es","ES"));

        try {
            tempDate = parseDateFromServer.parse(getJobOfferExpiration());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parseDateForShowDetail.format(tempDate);
    }*/
}
