package com.arqui.aulavirtual.data.entities;

import java.io.Serializable;

/**
 * Created by manu on 04/08/16.
 */
public class AccessTokenEntity implements Serializable {
    private String token;
    private String privatetoken;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPrivatetoken() {
        return privatetoken;
    }

    public void setPrivatetoken(String privatetoken) {
        this.privatetoken = privatetoken;
    }
}
