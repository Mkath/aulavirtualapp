package com.arqui.aulavirtual.data.entities;

import java.io.Serializable;
import java.util.ArrayList;

public class ModuleEntity implements Serializable {

    private int id;
    private String name;
    private String modplural;
    private String modicon;
    private ArrayList<ContentModuleEntity> contents;


    public String getModicon() {
        return modicon;
    }

    public void setModicon(String modicon) {
        this.modicon = modicon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModplural() {
        return modplural;
    }

    public void setModplural(String modplural) {
        this.modplural = modplural;
    }

    public ArrayList<ContentModuleEntity> getContents() {
        return contents;
    }

    public void setContents(ArrayList<ContentModuleEntity> contents) {
        this.contents = contents;
    }

    /*"id": 1,
                "url": "http://35.239.193.129/moodle/mod/forum/view.php?id=1",
                "name": "Avisos",
                "instance": 1,
                "visible": 1,
                "uservisible": true,
                "visibleoncoursepage": 1,
                "modicon": "http://35.239.193.129/moodle/theme/image.php/moove/forum/1559269261/icon",
                "modname": "forum",
                "modplural": "Foros",
                "availability": null,
                "indent": 0,
                "onclick": "",
                "afterlink": null,
                "customdata": "\"\"",
                "completion": 0*/

    /*/* public String getExpiration(){
        if (getJobOfferExpiration() == null ){
            return "";
        }
        Date tempDate = null;
        SimpleDateFormat parseDateFromServer= new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat parseDateForShowDetail =  new SimpleDateFormat("dd' de 'MMMM' del 'yyyy", new Locale("es","ES"));

        try {
            tempDate = parseDateFromServer.parse(getJobOfferExpiration());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parseDateForShowDetail.format(tempDate);
    }*/
}
