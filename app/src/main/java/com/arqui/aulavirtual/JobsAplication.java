package com.arqui.aulavirtual;

import android.app.Application;

import com.arqui.jobs.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by junior on 25/12/16.
 */

public class JobsAplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Gotham-Book.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );


    }





}
