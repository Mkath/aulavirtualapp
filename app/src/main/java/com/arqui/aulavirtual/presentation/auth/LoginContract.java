package com.arqui.aulavirtual.presentation.auth;

import com.arqui.aulavirtual.core.BasePresenter;
import com.arqui.aulavirtual.core.BaseView;
import com.arqui.aulavirtual.data.entities.AccessTokenEntity;
import com.arqui.aulavirtual.data.entities.UserEntity;

/**
 * Created by katherine on 12/05/17.
 */

public interface LoginContract {
    interface View extends BaseView<Presenter> {
        void loginSuccessful(UserEntity userEntity);
        void errorLogin(String msg);
        boolean isActive();
    }

    interface Presenter extends BasePresenter {
        void loginUser(String username, String password);
        void openSession(String token, UserEntity userEntity);
    }
}
