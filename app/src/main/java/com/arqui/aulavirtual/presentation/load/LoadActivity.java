package com.arqui.aulavirtual.presentation.load;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.arqui.aulavirtual.presentation.auth.LoginActivity;
import com.arqui.aulavirtual.presentation.main.homeworks.HomeworksActivity;
import com.arqui.aulavirtual.presentation.main.user.CoursesActivity;
import com.arqui.jobs.R;
import com.arqui.aulavirtual.core.BaseActivity;
import com.arqui.aulavirtual.data.local.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by katherine on 12/05/17.
 */

public class LoadActivity extends BaseActivity {
    @BindView(R.id.find_work)
    Button findWork;
    @BindView(R.id.find_company)
    Button findCompany;

    private SessionManager mSessionManager;


    // Set the duration of the splash screen

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loader);
        ButterKnife.bind(this);

        mSessionManager = new SessionManager(this);

        if(mSessionManager.isLogin()){
            switch (mSessionManager.getUserType()){
                case 1:
                    newActivityClearPreview(this, null, HomeworksActivity.class);
                    break;
                case 2:
                    newActivityClearPreview(this, null, CoursesActivity.class);
                    break;
            }
        }


    }


    @OnClick({R.id.find_work, R.id.find_company})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.find_work:
                Bundle bundle = new Bundle();
                bundle.putInt("userType", 2);
                newActivityClearPreview(this, bundle, LoginActivity.class);
                break;
            case R.id.find_company:
                Bundle bundle1 = new Bundle();
                bundle1.putInt("userType", 1);
                newActivityClearPreview(this, bundle1, LoginActivity.class);
                break;
        }
    }
}
