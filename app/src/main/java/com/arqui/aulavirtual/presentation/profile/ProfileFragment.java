package com.arqui.aulavirtual.presentation.profile;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arqui.jobs.R;
import com.arqui.aulavirtual.core.BaseFragment;
import com.arqui.aulavirtual.data.entities.UserEntity;
import com.arqui.aulavirtual.data.local.SessionManager;
import com.arqui.aulavirtual.presentation.load.LoadActivity;
import com.arqui.aulavirtual.utils.ProgressDialogCustom;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by katherine on 19/05/17.
 */

public class ProfileFragment extends BaseFragment {

    @BindView(R.id.photo_profile)
    CircleImageView photoProfile;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.container_photo)
    RelativeLayout containerPhoto;
    @BindView(R.id.ly_image_profile)
    RelativeLayout lyImageProfile;
    @BindView(R.id.im_name)
    ImageView imName;
    @BindView(R.id.tv_name_detail)
    EditText tvNameDetail;
    @BindView(R.id.ly_personal)
    LinearLayout lyPersonal;
    @BindView(R.id.im_email)
    ImageView imEmail;
    @BindView(R.id.tv_email_detail)
    EditText tvEmailDetail;
    @BindView(R.id.ly_email)
    LinearLayout lyEmail;
    @BindView(R.id.im_cel)
    ImageView imCel;
    @BindView(R.id.tv_apellido_detail)
    EditText tvApellidoDetail;
    @BindView(R.id.ly_apellido)
    LinearLayout lyApellido;
    @BindView(R.id.btn_send_email)
    Button btnSendEmail;
    @BindView(R.id.btn_close_sesion)
    Button btnCloseSesion;
    Unbinder unbinder;
    private SessionManager mSessionManager;
    private ProfileContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;
    private Bitmap mBitmap;
    private UserEntity userEntity;

    private String email;

    private boolean myProfile;

    public ProfileFragment() {
        // Requires empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public static ProfileFragment newInstance(Bundle bundle) {
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(getContext());

        myProfile = getArguments().getBoolean("myProfile");
        userEntity = (UserEntity) getArguments().getSerializable("userEntity");



    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Actualizando información...");

        if(!myProfile){
            btnCloseSesion.setVisibility(View.GONE);
            btnSendEmail.setVisibility(View.VISIBLE);
            btnSendEmail.setEnabled(true);
        }

        if(userEntity == null){
            email = mSessionManager.getUserEntity().getEmail();
            tvName.setText(mSessionManager.getUserEntity().getFullname());
            tvNameDetail.setText(mSessionManager.getUserEntity().getUsername());
            tvApellidoDetail.setText(mSessionManager.getUserEntity().getLastname());
            tvEmailDetail.setText(mSessionManager.getUserEntity().getEmail());
        }else{
            email = userEntity.getEmail();
            tvName.setText(userEntity.getFullname());
            tvNameDetail.setText(userEntity.getUsername());
            tvApellidoDetail.setText(userEntity.getLastname());
            tvEmailDetail.setText(userEntity.getEmail());
        }



    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void CloseSession() {
        mSessionManager.closeSession();
        newActivityClearPreview(getActivity(), null, LoadActivity.class);
    }

    @OnClick({R.id.btn_send_email, R.id.btn_close_sesion})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_send_email:
                sendEmail();
                break;
            case R.id.btn_close_sesion:
                CloseSession();
                break;
        }
    }

    protected void sendEmail() {
        String[] TO = {email}; //aquí pon tu correo
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
// Esto podrás modificarlo si quieres, el asunto y el cuerpo del mensaje
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Confirmación Entrevista");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Escribe aquí tu mensaje");

        try {
            startActivity(Intent.createChooser(emailIntent, "Enviar email..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(),
                    "No tienes clientes de email instalados.", Toast.LENGTH_SHORT).show();
        }
    }
}
