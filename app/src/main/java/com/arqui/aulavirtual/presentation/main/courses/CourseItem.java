package com.arqui.aulavirtual.presentation.main.courses;

import com.arqui.aulavirtual.data.entities.CourseEntity;

/**
 * Created by kath on 2/06/18.
 */

public interface CourseItem {

    void clickItem(CourseEntity jobEntity);

    void deleteItem(CourseEntity jobEntity, int position);
}
