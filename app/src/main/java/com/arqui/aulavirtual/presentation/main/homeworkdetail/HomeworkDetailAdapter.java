package com.arqui.aulavirtual.presentation.main.homeworkdetail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arqui.aulavirtual.core.LoaderAdapter;
import com.arqui.aulavirtual.data.entities.HomeworksEntity;
import com.arqui.aulavirtual.data.entities.ModuleEntity;
import com.arqui.aulavirtual.data.local.SessionManager;
import com.arqui.aulavirtual.presentation.main.homeworks.HomeworkItem;
import com.arqui.aulavirtual.utils.OnClickListListener;
import com.arqui.jobs.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by katherine on 31/05/17.
 */

public class HomeworkDetailAdapter extends LoaderAdapter<ModuleEntity> implements OnClickListListener {


    private Context context;
    private HomeworkDetailItem jobItem;
    private SessionManager mSessionManager;

    public HomeworkDetailAdapter(ArrayList<ModuleEntity> jobEntities, Context context, HomeworkDetailItem jobItem) {
        super(context);
        setItems(jobEntities);
        this.context = context;
        this.jobItem = jobItem;
        mSessionManager = new SessionManager(context);
    }

    public HomeworkDetailAdapter(ArrayList<ModuleEntity> jobEntities, Context context) {
        super(context);
        setItems(jobEntities);
        this.context = context;

    }

    public ArrayList<ModuleEntity> getItems() {
        return (ArrayList<ModuleEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return 1;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_homeworks_detail, parent, false);
        return new ViewHolder(root, this);
    }

    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
        ModuleEntity jobEntity = getItems().get(position);
//((ViewHolder) holder).cardView.setBackgroundResource(ticketEntity.getSchedules().getDestiny().getImage_1());
        if(jobEntity.getModplural().equals("Archivos")){
            ((ViewHolder) holder).containerArchivo.setVisibility(View.VISIBLE);
            ((ViewHolder) holder).containerTarea.setVisibility(View.GONE);
            ((ViewHolder) holder).tvNameArchivo.setText(jobEntity.getName());

        }else{
            ((ViewHolder) holder).tvName.setText(jobEntity.getName());
            ((ViewHolder) holder).containerArchivo.setVisibility(View.GONE);
            ((ViewHolder) holder).containerTarea.setVisibility(View.VISIBLE);
        }

        // ((ViewHolder) holder).tvVerTareas.setText(String.valueOf(jobEntity.getModules().size()) + "tareas");
        /*if (jobEntity.getUser_idUser() == mSessionManager.getUserEntity().getIdUser()) {
            ((ViewHolder) holder).tvAplicar.setText("Ver Candidatos");
        }*/
    }

    @Override
    public void onClick(int position) {
        ModuleEntity jobEntity = getItems().get(position);
        jobItem.clickItem(jobEntity);
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.container_tarea)
        LinearLayout containerTarea;
        @BindView(R.id.container_archivo)
        LinearLayout containerArchivo;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_name_archivo)
        TextView tvNameArchivo;

        @BindView(R.id.im_download)
        ImageView imDownload;
        @BindView(R.id.im_tarea)
        ImageView imTarea;
        private OnClickListListener onClickListListener;

        ViewHolder(View itemView, OnClickListListener onClickListListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.onClickListListener = onClickListListener;
            this.itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onClickListListener.onClick(getAdapterPosition());
        }
    }
}
