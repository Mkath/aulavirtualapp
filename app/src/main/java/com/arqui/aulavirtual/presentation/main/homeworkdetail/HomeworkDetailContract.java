package com.arqui.aulavirtual.presentation.main.homeworkdetail;


import com.arqui.aulavirtual.core.BasePresenter;
import com.arqui.aulavirtual.core.BaseView;
import com.arqui.aulavirtual.data.entities.HomeworksEntity;
import com.arqui.aulavirtual.data.entities.ModuleEntity;

import java.util.ArrayList;

/**
 * Created by katherine on 31/05/17.
 */

public interface HomeworkDetailContract {
    interface View extends BaseView<Presenter> {



        void clickItemDetail(ModuleEntity jobEntity);

        void responseSendData(String message);

        boolean isActive();



    }

    interface Presenter extends BasePresenter {


        void getPdf(int id);


    }
}
