package com.arqui.aulavirtual.presentation.main.homeworks;

import android.content.Context;

import com.arqui.aulavirtual.data.entities.CourseEntity;
import com.arqui.aulavirtual.data.entities.HomeworksEntity;
import com.arqui.aulavirtual.data.local.SessionManager;
import com.arqui.aulavirtual.data.remote.ServiceFactory;
import com.arqui.aulavirtual.data.remote.request.ListRequest;
import com.arqui.aulavirtual.data.remote.request.PostRequest;
import com.arqui.aulavirtual.presentation.main.courses.CourseContract;
import com.arqui.aulavirtual.presentation.main.courses.CourseItem;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 31/05/17.
 */

public class HomeworkPresenter implements HomeworkContract.Presenter, HomeworkItem {

    private final HomeworkContract.View mView;
    private final SessionManager mSessionManager;
    private Context context;
    private boolean firstLoad = false;
    private int currentPage = 1;

    public HomeworkPresenter(HomeworkContract.View mView, Context context) {
        this.mView = mView;
        this.mSessionManager = new SessionManager(context);
        this.mView.setPresenter(this);

    }



    @Override
    public void start() {
    }

    @Override
    public void loadListHomeworks(int id) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<ArrayList<HomeworksEntity>> reservation = listRequest.getHomeworks("f81bff7ea450e52b208d4a9e32a1cc01", "json", String.valueOf(id),"core_course_get_contents" );
        reservation.enqueue(new Callback<ArrayList<HomeworksEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<HomeworksEntity>> call, Response<ArrayList<HomeworksEntity>> response) {
                mView.setLoadingIndicator(false);
                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {

                    mView.getHomeworks(response.body());

                } else {
                    mView.showErrorMessage("Error al obtener la lista");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<HomeworksEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void clickItem(HomeworksEntity jobEntity) {
        mView.clickItemHomework(jobEntity);
    }

    @Override
    public void deleteItem(HomeworksEntity jobEntity, int position) {

    }
}
