package com.arqui.aulavirtual.presentation.register;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.arqui.aulavirtual.core.BaseActivity;
import com.arqui.aulavirtual.core.BaseFragment;
import com.arqui.aulavirtual.data.entities.UserEntity;
import com.arqui.jobs.R;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by katherine on 12/05/17.
 */

public class RegisterFragment extends BaseFragment implements RegisterContract.View, Validator.ValidationListener {

    @NotEmpty(message = "Este campo no puede ser vacío")
    @Length(max = 250, message = "Cantidad de dígitos no permitida", sequence = 1)
    @Email(message = "Email inválido")
    @BindView(R.id.et_email)
    EditText etEmail;

    @NotEmpty(message = "Este campo no puede ser vacío", sequence = 1)
    @Length(min = 6, max = 30, message = "La contraseña debe ser de al menos 6 dígitos", sequence = 2)
    @BindView(R.id.et_password)
    EditText etPassword;

    @Length(max = 50, message = "Cantidad de dígitos no permitida", sequence = 3)
    @NotEmpty(message = "Este campo no puede ser vacío", sequence = 4)
    @BindView(R.id.et_firstname)
    EditText etFirstname;


    @Length(max = 50, message = "Cantidad de dígitos no permitida", sequence = 5)
    @NotEmpty(message = "Este campo no puede ser vacío", sequence = 6)
    @BindView(R.id.et_lastname)
    EditText etLastname;

    @BindView(R.id.btn_create)
    Button btnCreate;
    Unbinder unbinder;

    @NotEmpty(message = "Este campo no puede ser vacío", sequence = 1)
    @Length(min = 6, max = 30, message = "La contraseña debe ser de al menos 6 dígitos", sequence = 2)
    @BindView(R.id.et_password_second)
    TextInputEditText etPasswordSecond;

    @BindView(R.id.et_phone)
    EditText etPhone;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;


    private Validator validator;
    private boolean isLoading = false;
    private ProgressDialog progressDialog;
    private RegisterContract.Presenter mPresenter;
    UserEntity userEntity;
    private String url;

    private int userType;


    public RegisterFragment() {
        // Requires empty public constructor
    }

    public static RegisterFragment newInstance(Bundle bundle) {
        RegisterFragment fragment = new RegisterFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_register, container, false);
        unbinder = ButterKnife.bind(this, root);

        userType = getArguments().getInt("userType");
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creando cuenta...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.circle_progress));
        validator = new Validator(this);
        validator.setValidationListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void registerSuccessful(final UserEntity userEntity) {
        showMessage("Registro exitoso");
        getActivity().finish();
    }

    @Override
    public void errorRegister(String msg) {
        showErrorMessage(msg);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(RegisterContract.Presenter mPresenter) {
        this.mPresenter = mPresenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {

        if (progressDialog != null) {

            if (active) {
                progressDialog.show();
            } else {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        }
    }

    @Override
    public void showMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);
    }

    @Override
    public void showErrorMessage(String message) {
        ((BaseActivity) getActivity()).showMessageError(message);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onValidationSucceeded() {

        if (etPassword.getText().toString().equals(etPasswordSecond.getText().toString())) {

            userEntity = new UserEntity();
            userEntity.setUsername(etFirstname.getText().toString());
            mPresenter.registerUser(userEntity);
            isLoading = true;

        } else {
            showMessage("Por favor ingresar las contraseñas iguales");
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick({ R.id.btn_create})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_create:
                validator.validate();
                break;
        }
    }

}
