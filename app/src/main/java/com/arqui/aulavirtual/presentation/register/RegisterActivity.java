package com.arqui.aulavirtual.presentation.register;

import android.os.Bundle;
import android.support.annotation.Nullable;


import com.arqui.aulavirtual.core.BaseActivity;
import com.arqui.aulavirtual.utils.ActivityUtils;
import com.arqui.jobs.R;

/**
 * Created by katherine on 12/05/17.
 */

public class RegisterActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clear);

        RegisterFragment fragment = (RegisterFragment) getSupportFragmentManager()
                .findFragmentById(R.id.body);

        if (fragment == null) {
            fragment = RegisterFragment.newInstance(getIntent().getExtras());

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.body);
        }

        new RegisterPresenter(fragment, this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}
