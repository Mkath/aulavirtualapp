package com.arqui.aulavirtual.presentation.main.user;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arqui.aulavirtual.data.entities.HomeworksEntity;
import com.arqui.aulavirtual.presentation.main.courses.CourseAdapter;
import com.arqui.aulavirtual.presentation.main.homeworks.HomeworksActivity;
import com.arqui.aulavirtual.presentation.profile.ProfileActivity;
import com.arqui.jobs.R;
import com.arqui.aulavirtual.core.BaseActivity;
import com.arqui.aulavirtual.core.BaseFragment;
import com.arqui.aulavirtual.core.RecyclerViewScrollListener;
import com.arqui.aulavirtual.core.ScrollChildSwipeRefreshLayout;
import com.arqui.aulavirtual.data.entities.CourseEntity;
import com.arqui.aulavirtual.data.local.SessionManager;
import com.arqui.aulavirtual.presentation.main.courses.CourseItem;
import com.arqui.aulavirtual.presentation.main.courses.CourseContract;
import com.arqui.aulavirtual.presentation.main.courses.CoursePresenter;
import com.arqui.aulavirtual.utils.ProgressDialogCustom;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by katherine on 31/05/17.
 */

public class CoursesFragment extends BaseFragment implements CourseContract.View {


    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.noListIcon)
    ImageView noListIcon;
    @BindView(R.id.noListMain)
    TextView noListMain;
    @BindView(R.id.noList)
    LinearLayout noList;
    @BindView(R.id.refresh_layout)
    ScrollChildSwipeRefreshLayout refreshLayout;
    Unbinder unbinder;
    @BindView(R.id.profile)
    FloatingActionButton profile;

    private CourseAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private CourseContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;

    private SessionManager mSessionManager;
    private AlertDialog dialogSend;

    public CoursesFragment() {
        // Requires empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadList();

    }

    public static CoursesFragment newInstance() {
        return new CoursesFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(getContext());
        mPresenter = new CoursePresenter(this, getContext());
        setHasOptionsMenu(true);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list, container, false);
        final ScrollChildSwipeRefreshLayout swipeRefreshLayout =
                (ScrollChildSwipeRefreshLayout) root.findViewById(R.id.refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.black),
                ContextCompat.getColor(getActivity(), R.color.dark_gray),
                ContextCompat.getColor(getActivity(), R.color.black)
        );
        // Set the scrolling view in the custom SwipeRefreshLayout.
        swipeRefreshLayout.setScrollUpChild(rvList);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //mPresenter.start();
                //mPresenter.loadList();
            }
        });

        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Obteniendo datos...");
        mLayoutManager = new LinearLayoutManager(getContext());
        rvList.setLayoutManager(mLayoutManager);
        mAdapter = new CourseAdapter(new ArrayList<CourseEntity>(), getContext(), (CourseItem) mPresenter);
        rvList.setAdapter(mAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_profile:
                //Toast.makeText(getActivity(), "Favoritos", Toast.LENGTH_SHORT).show();
                nextActivity(getActivity(), null, ProfileActivity.class, false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void getCourseList(ArrayList<CourseEntity> list) {
        mAdapter.setItems(list);
        if (list != null) {
            noList.setVisibility((list.size() > 0) ? View.GONE : View.VISIBLE);
        }

        rvList.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {
                //mPresenter.loadList();
            }
        });
    }

    @Override
    public void getHomeworks(ArrayList<HomeworksEntity> list) {

    }

    @Override
    public void clickItemCourse(CourseEntity jobEntity) {
        Bundle bundle = new Bundle();
        bundle.putInt("courseid", jobEntity.getId());
        nextActivity(getActivity(), bundle, HomeworksActivity.class, false);

        //sendDataForJob(jobEntity.getIdJobOffer());

    }

    @Override
    public void responseSendData(String message) {
        showMessage(message);
    }

    public void sendDataForJob(final int idJob) {
      /*  AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Hola, " + mSessionManager.getUserEntity().getUserName());
        builder.setMessage("¿Deseas enviar tus datos a este trabajo?");
        builder.setCancelable(false);
        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogSend.dismiss();
            }
        });
        builder.setPositiveButton("ENVIAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {

                mPresenter.sendDatoForJob(idJob, mSessionManager.getUserEntity().getIdUser());

            }
        });
        dialogSend = builder.create();
        dialogSend.show();*/
    }


    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(CourseContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(final boolean active) {
        if (getView() == null) {
            return;
        }
        final SwipeRefreshLayout srl =
                (SwipeRefreshLayout) getView().findViewById(R.id.refresh_layout);

        // Make sure setRefreshing() is called after the layout is done with everything else.
        srl.post(new Runnable() {
            @Override
            public void run() {
                srl.setRefreshing(active);
            }
        });

        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);
    }

    @Override
    public void showErrorMessage(String message) {
        ((BaseActivity) getActivity()).showMessageError(message);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    @OnClick(R.id.profile)
    public void onViewClicked() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("myProfile", true);
        nextActivity(getActivity(), bundle, ProfileActivity.class, false);
    }
}
