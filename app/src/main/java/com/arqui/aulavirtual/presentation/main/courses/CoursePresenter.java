package com.arqui.aulavirtual.presentation.main.courses;

import android.content.Context;


import com.arqui.aulavirtual.data.entities.CourseEntity;
import com.arqui.aulavirtual.data.entities.HomeworksEntity;
import com.arqui.aulavirtual.data.entities.trackholder.TrackEntityHolder;
import com.arqui.aulavirtual.data.local.SessionManager;
import com.arqui.aulavirtual.data.remote.ServiceFactory;
import com.arqui.aulavirtual.data.remote.request.ListRequest;
import com.arqui.aulavirtual.data.remote.request.PostRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 31/05/17.
 */

public class CoursePresenter implements CourseContract.Presenter, CourseItem {

    private final CourseContract.View mView;
    private final SessionManager mSessionManager;
    private Context context;
    private boolean firstLoad = false;
    private int currentPage = 1;

    public CoursePresenter(CourseContract.View mView, Context context) {
        this.mView = mView;
        this.mSessionManager = new SessionManager(context);
        this.mView.setPresenter(this);

    }
    @Override
    public void clickItem(CourseEntity jobEntity) {
        mView.clickItemCourse(jobEntity);

    }

    @Override
    public void deleteItem(CourseEntity jobEntity, int position) {

    }


    @Override
    public void start() {
    }

    @Override
    public void loadListHomeworks(int id) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<ArrayList<HomeworksEntity>> reservation = listRequest.getHomeworks("f81bff7ea450e52b208d4a9e32a1cc01", "json", String.valueOf(id),"core_course_get_courses" );
        reservation.enqueue(new Callback<ArrayList<HomeworksEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<HomeworksEntity>> call, Response<ArrayList<HomeworksEntity>> response) {
                mView.setLoadingIndicator(false);
                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {

                    mView.getHomeworks(response.body());

                } else {
                    mView.showErrorMessage("Error al obtener la lista");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<HomeworksEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void loadList() {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<ArrayList<CourseEntity>> reservation = listRequest.getCourses("f81bff7ea450e52b208d4a9e32a1cc01", "json", "core_enrol_get_users_courses", mSessionManager.getUserEntity().getId());
        reservation.enqueue(new Callback<ArrayList<CourseEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<CourseEntity>> call, Response<ArrayList<CourseEntity>> response) {
                mView.setLoadingIndicator(false);
                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {

                    mView.getCourseList(response.body());

                } else {
                    mView.showErrorMessage("Error al obtener la lista");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<CourseEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void sendDatoForJob(int idJob, int idUser) {
         mView.setLoadingIndicator(true);
        PostRequest postRequest = ServiceFactory.createService(PostRequest.class);
        final Call<Void> reservation = postRequest.sendMyAplication(idUser, idJob);
        reservation.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                mView.setLoadingIndicator(false);
                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {

                    if(response.code() == 201){
                        mView.responseSendData("Se ha asignado correctamente");
                    }else{
                        mView.showErrorMessage("Error al obtener la lista");
                    }

                } else {
                    mView.showErrorMessage("Error al obtener la lista");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }
}
