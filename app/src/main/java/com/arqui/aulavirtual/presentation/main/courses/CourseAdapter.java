package com.arqui.aulavirtual.presentation.main.courses;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arqui.jobs.R;
import com.arqui.aulavirtual.core.LoaderAdapter;
import com.arqui.aulavirtual.data.entities.CourseEntity;
import com.arqui.aulavirtual.data.local.SessionManager;
import com.arqui.aulavirtual.utils.OnClickListListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by katherine on 31/05/17.
 */

public class CourseAdapter extends LoaderAdapter<CourseEntity> implements OnClickListListener {


    private Context context;
    private CourseItem jobItem;
    private SessionManager mSessionManager;

    public CourseAdapter(ArrayList<CourseEntity> jobEntities, Context context, CourseItem jobItem) {
        super(context);
        setItems(jobEntities);
        this.context = context;
        this.jobItem = jobItem;
        mSessionManager = new SessionManager(context);
    }

    public CourseAdapter(ArrayList<CourseEntity> jobEntities, Context context) {
        super(context);
        setItems(jobEntities);
        this.context = context;

    }

    public ArrayList<CourseEntity> getItems() {
        return (ArrayList<CourseEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return 1;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_courses, parent, false);
        return new ViewHolder(root, this);
    }

    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
        CourseEntity jobEntity = getItems().get(position);
        //((ViewHolder) holder).cardView.setBackgroundResource(ticketEntity.getSchedules().getDestiny().getImage_1());
        ((ViewHolder) holder).tvName.setText(jobEntity.getFullname());

        /*if (jobEntity.getUser_idUser() == mSessionManager.getUserEntity().getIdUser()) {
            ((ViewHolder) holder).tvAplicar.setText("Ver Candidatos");
        }*/
    }

    @Override
    public void onClick(int position) {
        CourseEntity jobEntity = getItems().get(position);
        jobItem.clickItem(jobEntity);
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_descript)
        TextView tvDescript;
        @BindView(R.id.tv_expirate)
        TextView tvExpirate;
        @BindView(R.id.tv_aplicar)
        TextView tvAplicar;
        private OnClickListListener onClickListListener;

        ViewHolder(View itemView, OnClickListListener onClickListListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.onClickListListener = onClickListListener;
            this.itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onClickListListener.onClick(getAdapterPosition());
        }
    }
}
